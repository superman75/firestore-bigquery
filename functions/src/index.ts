import * as functions from 'firebase-functions';
const {BigQuery} = require('@google-cloud/bigquery');
const bigquery = new BigQuery();
const boxSchema = [
	{name: 'id', type: 'STRING', mode: 'required'},
	{name: 'brand', type: 'STRING'},
	{name: 'epoch_time', type: 'FLOAT'},
	{name: 'exp_date', type: 'STRING'},
	{name: 'exp_month', type: 'STRING'},
	{name: 'exp_year', type: 'STRING'},
	{name: 'latitude', type: 'STRING'},
	{name: 'longitude', type: 'STRING'},
	{name: 'model_info', type: 'STRING'},
	{name: 'qty', type: 'INTEGER'},
	{name: 'serial_number', type: 'STRING'},
	{name: 'timestamp', type: 'STRING'},
	{name: 'user', type: 'STRING'}
]
const datasetId = "jnj_firestore"
exports.boxWriteListner = functions.firestore.document('Box/{boxId}').onWrite(
	async(change, context) => {
		const docId = context.params.boxId;
		const box_after = change.after.exists ? change.after.data() : null;
		const box_before = change.before.exists ? change.before.data() : null;

	    
	    const tableName = "Box";
		const dataset = await bigquery.dataset(datasetId);
		dataset.exists().catch((err: any) => {
			console.error(`dataset.exists: dataset ${datasetId} does not exist: ${JSON.stringify(
		      err
		    )}`)
		    return err;
		});

		try {
			await dataset.table(tableName).get();			
		} catch {
			const option = {
				schema: boxSchema,
				location: 'US'
			}
			await dataset.createTable(tableName, option);	
			// console.log(`Table ${tableName} created`);	
		}

		// new box documents created
		if(!box_before && box_after) {
			// console.log(`create box for document id : ${docId}`);
			let target_name = 'id, ';
			let values = `'${docId}', `;

			Object.keys(box_after).forEach((key: any) => {
				target_name += key + ', ';
				if(typeof box_after[key] === 'string') {
					values = values + `'${box_after[key]}', `
				} else {
					values = values + `${box_after[key]}, `
				}
			});

			target_name = target_name.substring(0, target_name.length - 2);
			values = values.substring(0, values.length - 2);

			const createQuery = `INSERT ${datasetId}.${tableName} (${target_name}) VALUES(${values})`
			// console.log(createQuery);

			const options = {
				query: createQuery,
				timeoutMs: 100000,
				useLegacySql: false,
			}
			await bigquery.query(options);
			return;
		}
		// box documents deleted
		if(!box_after) {
			// console.log(`delete box for document id : ${docId}`);
			const deleteQuery = `DELETE FROM ${datasetId}.${tableName} WHERE id='${docId}'`;
			// console.log(deleteQuery);

			const options = {
				query: deleteQuery,
				timeoutMs: 100000,
				useLegacySql: false,
			}
			await bigquery.query(options);
			return;
		}

		// box documents updated
		let setValues = '';
		Object.keys(box_after).forEach((key: any) => {
			if(!box_before || box_after[key] !== box_before[key]){
				if(typeof box_after[key] === 'string') {
					setValues = setValues + `${key} = '${box_after[key]}', `
				} else {
					setValues = setValues + `${key} = ${box_after[key]}, `
				}				
			}
		});

		if(setValues.length > 0) {	
			setValues = setValues.substring(0, setValues.length - 2);		
			const updateQuery = `UPDATE ${datasetId}.${tableName} SET ${setValues} WHERE id='${docId}'`;		
			const options = {
				query: updateQuery,
				timeoutMs: 100000,
				useLegacySql: false,
			}
			await bigquery.query(options);
		}
		return;		
	})


exports.formWriteListner = functions.firestore.document('Form/{formId}').onWrite(
	async(change, context) => {
		const docId = context.params.formId;
		const form_after = change.after.exists ? change.after.data() : null;
		const form_before = change.before.exists ? change.before.data() : null;

	    const tableName = "Form";
		const dataset = await bigquery.dataset(datasetId);
		dataset.exists().catch((err: any) => {
			console.error(`dataset.exists: dataset ${datasetId} does not exist: ${JSON.stringify(
		      err
		    )}`)
		    return err;
		});

		try {
			await dataset.table(tableName).get();			
		} catch {
			const option = {
				schema: boxSchema,
				location: 'US'
			}
			await dataset.createTable(tableName, option);	
			// console.log(`Table ${tableName} created`);	
		}

		// new form documents created
		if(!form_before && form_after) {
			// console.log(`create form for document id : ${docId}`);
			let target_name = 'id, ';
			let values = `'${docId}', `;

			Object.keys(form_after).forEach((key: any) => {
				target_name += key + ', ';
				if(typeof form_after[key] === 'string') {
					values = values + `'${form_after[key]}', `
				} else {
					values = values + `${form_after[key]}, `
				}
			});

			target_name = target_name.substring(0, target_name.length - 2);
			values = values.substring(0, values.length - 2);

			const createQuery = `INSERT ${datasetId}.${tableName} (${target_name}) VALUES(${values})`
			// console.log(createQuery);

			const options = {
				query: createQuery,
				timeoutMs: 100000,
				useLegacySql: false,
			}
			await bigquery.query(options);
			return;
		}
		// form documents deleted
		if(!form_after) {
			// console.log(`delete form for document id : ${docId}`);
			const deleteQuery = `DELETE FROM ${datasetId}.${tableName} WHERE id='${docId}'`;
			// console.log(deleteQuery);

			const options = {
				query: deleteQuery,
				timeoutMs: 100000,
				useLegacySql: false,
			}
			await bigquery.query(options);
			return;
		}

		// form documents updated
		let setValues = '';
		Object.keys(form_after).forEach((key: any) => {
			if(!form_before || form_after[key] !== form_before[key]){
				if(typeof form_after[key] === 'string') {
					setValues = setValues + `${key} = '${form_after[key]}', `
				} else {
					setValues = setValues + `${key} = ${form_after[key]}, `
				}				
			}
		});

		if(setValues.length > 0) {	
			setValues = setValues.substring(0, setValues.length - 2);		
			const updateQuery = `UPDATE ${datasetId}.${tableName} SET ${setValues} WHERE id='${docId}'`;		
			const options = {
				query: updateQuery,
				timeoutMs: 100000,
				useLegacySql: false,
			}
			await bigquery.query(options);
		}
		return;		
	})